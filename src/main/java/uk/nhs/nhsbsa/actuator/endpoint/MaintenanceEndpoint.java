package uk.nhs.nhsbsa.actuator.endpoint;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.info.Info;
import org.springframework.stereotype.Component;

/** Spring actuator endpoint entry point for maintenance schedule data. */
@Endpoint(id = "maintenance", enableByDefault = true)
@Component("maintenanceEndpoint")
public class MaintenanceEndpoint {

  @Autowired MaintenanceInfoContributor maintenanceInfoContributor;

  /**
   * Basic constructor.
   *
   * @param maintenanceInfoContributor the contributor instance.
   */
  public MaintenanceEndpoint(MaintenanceInfoContributor maintenanceInfoContributor) {
    this.maintenanceInfoContributor = maintenanceInfoContributor;
  }

  /**
   * Operation when a GET is called from the endpoint.
   *
   * @return the built information for the request.
   */
  @ReadOperation
  public Map<String, Object> maintenanceSchedule() {
    var builder = new Info.Builder();
    maintenanceInfoContributor.contribute(builder);
    Info build = builder.build();
    return build.getDetails();
  }
}
