package uk.nhs.nhsbsa.actuator.endpoint;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;
import uk.nhs.nhsbsa.actuator.model.MaintenanceSchedule;
import uk.nhs.nhsbsa.actuator.repository.MaintenanceScheduleRepositoryImpl;

/** Endpoint contributor. This essentially pulls the data together for the output builder. */
@Component
public class MaintenanceInfoContributor implements InfoContributor {

  @Autowired MaintenanceScheduleRepositoryImpl maintenanceScheduleRepository;

  @Value("${management.endpoints.maintenance.key:myApp}")
  private String applicationKey;

  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

  public MaintenanceInfoContributor(
      MaintenanceScheduleRepositoryImpl maintenanceScheduleRepository) {
    this.maintenanceScheduleRepository = maintenanceScheduleRepository;
  }

  @Override
  public void contribute(Info.Builder builder) {
    Map<String, Object> maintenanceSchedule = new HashMap<>();

    Optional<MaintenanceSchedule> optionalSchedule =
        maintenanceScheduleRepository.findByApplication(applicationKey);

    if (optionalSchedule.isPresent()) {
      maintenanceSchedule.put("application", optionalSchedule.get().getApplication());
      maintenanceSchedule.put("scheduled", Boolean.toString(optionalSchedule.get().isScheduled()));
      maintenanceSchedule.put("startDate", optionalSchedule.get().getStartDate().format(formatter));
      maintenanceSchedule.put("startTime", optionalSchedule.get().getStartTime());
      maintenanceSchedule.put("endTime", optionalSchedule.get().getEndTime());
    }
    builder.withDetail("maintenanceSchedule", maintenanceSchedule);
  }

  public void setApplicationKey(String applicationKey) {
    this.applicationKey = applicationKey;
  }
}
