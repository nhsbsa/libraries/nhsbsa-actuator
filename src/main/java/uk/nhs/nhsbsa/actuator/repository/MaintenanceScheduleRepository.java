package uk.nhs.nhsbsa.actuator.repository;

import java.util.Optional;
import uk.nhs.nhsbsa.actuator.model.MaintenanceSchedule;

/** Repository Interface. */
public interface MaintenanceScheduleRepository {

  Optional<MaintenanceSchedule> findByApplication(String application);
}
