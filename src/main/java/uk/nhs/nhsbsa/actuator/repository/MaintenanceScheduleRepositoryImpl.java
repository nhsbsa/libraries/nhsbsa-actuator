package uk.nhs.nhsbsa.actuator.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import uk.nhs.nhsbsa.actuator.model.MaintenanceSchedule;

/**
 * So it was decided to go with a simple JDBC template as this requires fewer dependencies and
 * configuration on the client. It also eliminates the need to Implement the JPA methods so that the
 * repository can be used to wire together as a simple component and also does not require the
 * MaintenanceSchedule entity to be additionally scanned by the using application.
 *
 * <p>So the approach taken was to require min dependencies and config by any client apps
 */
@Component
public class MaintenanceScheduleRepositoryImpl implements MaintenanceScheduleRepository {

  @Autowired NamedParameterJdbcTemplate jdbcTemplate;

  private static final String MAINTENANCE_SCHEDULE_CHECK_SQL =
      "SELECT application_name, scheduled, start_date, start_time, end_time"
          + " FROM maintenance_schedule ms"
          + " WHERE application_name = :applicationKey"
          + " AND scheduled is true"
          + " AND start_date > NOW() LIMIT 1";

  public MaintenanceScheduleRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  /**
   * Method to pull the schedule from the DB as a single entity as this is how it should be
   * configured.
   *
   * @param applicationKey the application Key as defined in the DB for the application.
   * @return optional maintenanceSchedule.
   */
  public Optional<MaintenanceSchedule> findByApplication(String applicationKey) {
    try {
      SqlParameterSource namedParameters =
          new MapSqlParameterSource("applicationKey", applicationKey);
      return Optional.ofNullable(
          jdbcTemplate.queryForObject(
              MAINTENANCE_SCHEDULE_CHECK_SQL, namedParameters, new MaintenanceScheduleRowMapper()));
    } catch (DataAccessException emptyResultDataAccessException) {
      return Optional.empty();
    }
  }

  /** Simple row mapper to scan the resultset and drop into DTO. */
  protected static class MaintenanceScheduleRowMapper implements RowMapper<MaintenanceSchedule> {

    @Override
    public MaintenanceSchedule mapRow(ResultSet rs, int rowNum) throws SQLException {
      var maintenanceSchedule = new MaintenanceSchedule();
      maintenanceSchedule.setApplication(rs.getString(1));
      maintenanceSchedule.setScheduled(rs.getBoolean(2));
      maintenanceSchedule.setStartDate(rs.getTimestamp(3).toLocalDateTime());
      maintenanceSchedule.setStartTime(rs.getString(4));
      maintenanceSchedule.setEndTime(rs.getString(5));
      return maintenanceSchedule;
    }
  }
}
