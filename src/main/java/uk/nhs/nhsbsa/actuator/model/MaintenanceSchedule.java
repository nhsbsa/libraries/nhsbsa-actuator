package uk.nhs.nhsbsa.actuator.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Maintenance Schedule DTO. */
@Getter
@Setter
@NoArgsConstructor
public class MaintenanceSchedule implements Serializable {

  private String application;
  private boolean isScheduled = false;
  private LocalDateTime startDate;
  private String startTime;
  private String endTime;
}
