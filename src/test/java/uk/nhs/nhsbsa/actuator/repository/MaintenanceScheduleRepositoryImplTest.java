package uk.nhs.nhsbsa.actuator.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import uk.nhs.nhsbsa.actuator.model.MaintenanceSchedule;
import uk.nhs.nhsbsa.actuator.repository.MaintenanceScheduleRepositoryImpl.MaintenanceScheduleRowMapper;

class MaintenanceScheduleRepositoryImplTest {

  @Mock NamedParameterJdbcTemplate jdbcTemplate;

  private MaintenanceSchedule maintenanceSchedule;

  private MaintenanceScheduleRepositoryImpl maintenanceScheduleRepository;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    maintenanceScheduleRepository = new MaintenanceScheduleRepositoryImpl(jdbcTemplate);

    maintenanceSchedule = new MaintenanceSchedule();
    maintenanceSchedule.setApplication("myApp");
    maintenanceSchedule.setScheduled(true);
  }

  @Test
  void findByApplication() {

    when(jdbcTemplate.queryForObject(
            anyString(), any(SqlParameterSource.class), any(MaintenanceScheduleRowMapper.class)))
        .thenReturn(maintenanceSchedule);

    Optional<MaintenanceSchedule> result = maintenanceScheduleRepository.findByApplication("myApp");

    assertEquals("myApp", result.get().getApplication());
  }

  /**
   * Test for when multiple rows are returned that would indicate misconfig of the data in the
   * tables against the selection criteria.
   */
  @Test
  void verifyInvalidResults() {

    when(jdbcTemplate.queryForObject(
            anyString(), any(SqlParameterSource.class), any(MaintenanceScheduleRowMapper.class)))
        .thenThrow(EmptyResultDataAccessException.class);

    Optional<MaintenanceSchedule> result = maintenanceScheduleRepository.findByApplication("myApp");

    assertTrue(result.isEmpty());
  }

  /** Test for potential missing tables of more general data access exception. */
  @Test
  void missingDatabaseTableTest() {

    when(jdbcTemplate.queryForObject(
            anyString(), any(SqlParameterSource.class), any(MaintenanceScheduleRowMapper.class)))
        .thenThrow(PermissionDeniedDataAccessException.class);

    Optional<MaintenanceSchedule> result = maintenanceScheduleRepository.findByApplication("myApp");

    assertTrue(result.isEmpty());
  }

  @Test
  void maintenanceScheduleRowMapper() throws SQLException {

    LocalDateTime now = LocalDateTime.now();
    MaintenanceScheduleRepositoryImpl.MaintenanceScheduleRowMapper maintenanceScheduleRowMapper =
        new MaintenanceScheduleRepositoryImpl.MaintenanceScheduleRowMapper();
    ResultSet resultSet = mock(ResultSet.class);

    when(resultSet.getBoolean(2)).thenReturn(true);
    when(resultSet.getTimestamp(3)).thenReturn(java.sql.Timestamp.valueOf(now));
    when(resultSet.getString(anyInt())).thenReturn("Value");

    MaintenanceSchedule maintenanceSchedule = maintenanceScheduleRowMapper.mapRow(resultSet, 1);
    assertNotNull(maintenanceSchedule);
    assertTrue(maintenanceSchedule.isScheduled());
    assertEquals("Value", maintenanceSchedule.getStartTime());
    assertEquals("Value", maintenanceSchedule.getEndTime());
    assertEquals(now, maintenanceSchedule.getStartDate());
  }
}
