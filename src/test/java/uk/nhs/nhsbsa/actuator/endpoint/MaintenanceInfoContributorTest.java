package uk.nhs.nhsbsa.actuator.endpoint;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.Info.Builder;
import uk.nhs.nhsbsa.actuator.model.MaintenanceSchedule;
import uk.nhs.nhsbsa.actuator.repository.MaintenanceScheduleRepositoryImpl;

class MaintenanceInfoContributorTest {

  @Mock private MaintenanceScheduleRepositoryImpl maintenanceScheduleRepository;

  // @InjectMocks
  private MaintenanceInfoContributor maintenanceInfoContributor;

  private Info.Builder builder = new Builder();
  private MaintenanceSchedule maintenanceSchedule;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    maintenanceInfoContributor = new MaintenanceInfoContributor(maintenanceScheduleRepository);
    maintenanceInfoContributor.setApplicationKey("myApp");

    maintenanceSchedule = new MaintenanceSchedule();
    maintenanceSchedule.setApplication("myApp");
    maintenanceSchedule.setScheduled(true);
    maintenanceSchedule.setStartDate(LocalDateTime.now().plusDays(5));
    maintenanceSchedule.setStartTime("16:00");
    maintenanceSchedule.setEndTime("18:00");
  }

  @Test
  void contribute() {

    when(maintenanceScheduleRepository.findByApplication(anyString()))
        .thenReturn(Optional.ofNullable(maintenanceSchedule));

    maintenanceInfoContributor.contribute(builder);

    System.out.println("builder : " + builder.build().toString());
    assertNotNull(builder.build().toString());

    HashMap<String, String> result =
        (HashMap) builder.build().getDetails().get("maintenanceSchedule");
    assertEquals("myApp", result.get("application"));
    assertEquals("true", result.get("scheduled"));
    assertEquals("16:00", result.get("startTime"));
    assertEquals("18:00", result.get("endTime"));
  }

  @Test
  void contributeNoInformationFound() {

    when(maintenanceScheduleRepository.findByApplication(anyString())).thenReturn(Optional.empty());

    maintenanceInfoContributor.contribute(builder);

    assertNotNull(builder.build().toString());
    assertEquals("{maintenanceSchedule={}}", builder.build().toString());

    HashMap<String, String> result =
        (HashMap) builder.build().getDetails().get("maintenanceSchedule");
    assertNull(result.get("application"));
  }
}
