package uk.nhs.nhsbsa.actuator.endpoint;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uk.nhs.nhsbsa.actuator.model.MaintenanceSchedule;
import uk.nhs.nhsbsa.actuator.repository.MaintenanceScheduleRepositoryImpl;

class MaintenanceEndpointTest {

  @Mock private MaintenanceScheduleRepositoryImpl maintenanceScheduleRepository;

  private MaintenanceSchedule maintenanceSchedule;
  private MaintenanceEndpoint maintenanceEndpoint;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    MaintenanceInfoContributor maintenanceInfoContributor =
        new MaintenanceInfoContributor(maintenanceScheduleRepository);
    maintenanceInfoContributor.setApplicationKey("myApp");
    maintenanceEndpoint = new MaintenanceEndpoint(maintenanceInfoContributor);

    maintenanceSchedule = new MaintenanceSchedule();
    maintenanceSchedule.setApplication("myApp");
    maintenanceSchedule.setScheduled(true);
    maintenanceSchedule.setStartDate(LocalDateTime.now().plusDays(5));
    maintenanceSchedule.setStartTime("16:00");
    maintenanceSchedule.setEndTime("18:00");
  }

  @Test
  void maintenanceSchedule() {

    when(maintenanceScheduleRepository.findByApplication(anyString()))
        .thenReturn(Optional.ofNullable(maintenanceSchedule));

    Map<String, Object> detail = maintenanceEndpoint.maintenanceSchedule();

    HashMap<String, String> result = (HashMap) detail.get("maintenanceSchedule");

    assertEquals("myApp", result.get("application"));
    assertEquals("true", result.get("scheduled"));
    assertEquals("16:00", result.get("startTime"));
    assertEquals("18:00", result.get("endTime"));
  }
}
