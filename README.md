# BSA Spring Actuator endpoint for planned maintenance

Provides a library for inclusion in a Spring Boot application to 
add an additional actuator endpoint to provide access to maintenance information per project.

## Dependency management

### Maven

Add the following dependency to your project:

```
<dependency>
    <groupId>uk.nhs.nhsbsa</groupId>
    <artifactId>bsa-actuator</artifactId>
    <version>${bsa-actuator.version}</version>
</dependency>
```


## Configuration

Configuration properties are used to configure the exposure of the endpoint in the Spring 
application context. This should be at the root level of your application.yaml file.
The key property is to be related to your application and is used in 
retrieving distinct application details. This relates directly to the name given in the 
applicationName field of the schedule data inserted into the database.

```
# Required Config
management:
  endpoints:
    maintenance:
      key: myApp
    web:
      exposure:
        include: health,info,maintenance
    
```
Inside the jar package and within the gitlab repo is a liquibase file that should be included 
into your projects changeset. This create the tables required for correct operation of the feature.

```
<include file="schema/bsa-actuator-changelog-1.0.xml" relativeToChangelogFile="true"/>
```

All application wiring is present in the jar but may require your application to scan for 
components in the following package like below. The initial entry would be your application root.

```
@ComponentScan(basePackages = {"uk.nhs.nhsbsa","uk.nhs.nhsbsa.actuator"})
```

Most bsa applications that are secured operate an open policy for actuator endpoints as 
they are used for health monitoring. Therefore the url should be unprotected. 
However, this will need to be opened up if not already available in your security config.

## Data

An example data script to populate the maintenance schedule table is available in 
the gitlab repo and is also within the jar file. This is best performed by DBA/OPS to mark 
the application as having a maintenance schedule window.

```
INSERT INTO public.maintenance_schedule(
    id, application_name, scheduled, start_date, start_time, end_time)
VALUES (default, 'myApp', true, '2023-05-05 19:00:00', '18:00', '20:00');
```

## Usage

The following data is presented back to the calling client to be used in any presentation 
such as a maintenance banner within your front end UI.

if a Spring Boot `RestTemplateBuilder` bean is present. 

http://yourhosturl/actuator/maintenance

```
{
"maintenanceSchedule":{
    "application":"myApp",
    "scheduled":"true",
    "startTime":"18:00",
    "endTime":"20:00",
    "startDate":"05-05-2023 19:00"
    }
}
```


